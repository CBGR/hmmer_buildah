#!/bin/sh

# SPDX-FileCopyrightText: 2020-2022 University of Oslo
# SPDX-FileContributor: Paul Boddie <paul.boddie@ncmm.uio.no>
# SPDX-License-Identifier: GPL-3.0-or-later

set -e

PROGNAME=$(basename "$0")
THISDIR=$(dirname "$0")
BASEDIR=$(realpath "$THISDIR/..")
DIRNAME=$(basename "$BASEDIR")
WORKDIR="$BASEDIR/deploy/work"

# Archive location and details.

DISTDIR="hmmer-3.3.2"
FILENAME="${DISTDIR}.tar.gz"
URL="http://eddylab.org/software/hmmer/$FILENAME"

# Container details.

CONTAINER=$1

# Require a container.

if [ ! "$CONTAINER" ] || [ "$CONTAINER" = '--help' ] ; then
    cat 1>&2 <<EOF
Usage: $PROGNAME <container>

Build the hmmer software inside the indicated container. Typically, this script
is invoked by the more general build script.
EOF
    exit 1
fi

# Cache downloads outside the container.

if [ ! -e "$WORKDIR" ] ; then
    mkdir "$WORKDIR"
fi

# Fetch the archive if necessary.

ARCHIVE="$WORKDIR/$FILENAME"

if [ ! -e "$ARCHIVE" ] ; then
    wget -P "$WORKDIR" "$URL"
fi

# Access the container.

MNT=$(buildah mount "$CONTAINER")

# Define paths relative to the container.

APP="$MNT/var/www/apps/$DIRNAME"
APP_WORKDIR="$APP/deploy/work"

if [ ! -e "$APP_WORKDIR" ] ; then
    mkdir "$APP_WORKDIR"
fi

if [ ! -e "$APP_WORKDIR/$DISTDIR" ] ; then
    tar zxf "$ARCHIVE" -C "$APP_WORKDIR"
fi

buildah umount "$CONTAINER"

# Build and install hmmer inside the container.

APP="/var/www/apps/$DIRNAME"
WORKDIR="$APP/deploy/work"

buildah run "$CONTAINER" "$APP/deploy/tools/build_hmmer" "$WORKDIR/$DISTDIR" "$APP/hmmer"

# vim: tabstop=4 expandtab shiftwidth=4
