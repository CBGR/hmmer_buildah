HMMER Packaging
===============

This repository provides tools for packaging HMMER for deployment, potentially
in a container.

To install the necessary software to use these tools:

.. code:: shell

  ./deploy.sh

This installs the packages listed in ``requirements-sys-container.sh``.

To build and package HMMER for a system or container of a particular operating
system distribution:

.. code:: shell

  container/container-init.sh centos:8 hmmer
  container/container-build.sh hmmer
  container/container-package.sh hmmer

The above commands respectively initialise a build container using a base
image (``centos:8`` indicated here), build the software within the container,
and extract the built software as an archive.

As a result, an archive containing the built software should be produced:

::

  hmmer.tar.gz

The archive can be unpacked into a system or container of the same type as
that chosen for the base image.
